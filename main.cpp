#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;
#endif /* __PROGTEST__ */

struct CComponent {
    virtual void Print(ostream & os,const map<int,bool> & levels, bool isLast = false) const = 0;
};

class CAddress: public CComponent {
    string m_address;
public:
    CAddress(const string &addr);
    const string& getName() const;
    void Print(ostream & os,const map<int,bool> & levels, bool isLast = false) const override;
};

CAddress::CAddress(const string &addr): m_address(addr) {

}

void CAddress::Print(ostream & os,const map<int,bool> & levels, bool isLast) const {
    for(auto & level : levels){
        os << (level.second ? "| " : "  ");
    }
    os << (isLast ? "\\-" : "+-");
    os << m_address << endl;
}

const string &CAddress::getName() const {
    return m_address;
}


class CCPU: public CComponent {
    int m_cores;
    int m_frequency;
public:
    CCPU(int cores, int frequency);

    void Print(ostream & os,const map<int,bool> & levels, bool isLast = false) const override;
};

CCPU::CCPU(int cores, int frequency): m_cores(cores), m_frequency(frequency) {

}

void CCPU::Print(ostream &os, const map<int, bool> &levels, bool isLast) const {
    for(auto & level : levels){
        os << (level.second ? "| " : "  ");
    }
    os << (isLast ? "\\-" : "+-");
    os << "CPU, " << m_cores << " cores @ " << m_frequency << "MHz" << endl;
}
struct Partition {
    int size;
    string name;
    Partition(int size, const string& name);
};

class CDisk: public CComponent {
    string m_type;
    int m_size;
    list<Partition> m_partitions;
public:
    static const string MAGNETIC;
    static const string SSD;
    CDisk(const string & type, int size);
    CDisk &AddPartition(int size, const string& name);
    void Print(ostream &os, const map<int, bool> &levels, bool isLast) const override;
};
const string CDisk::MAGNETIC = "HDD";
const string CDisk::SSD = "SSD";

CDisk::CDisk(const string &type, int size): m_type(type), m_size(size) {

}

CDisk &CDisk::AddPartition(int size, const string &name) {
    m_partitions.emplace_back(Partition(size,name));
    return *this;
}


Partition::Partition(int size, const string &name): size(size),name(name) {
}

void CDisk::Print(ostream &os, const map<int, bool> &levels, bool isLast) const {
    string prepend;
    for(auto & level : levels){
        prepend += (level.second ? "| " : "  ");
    }
    os << prepend;
    os << (isLast ? "\\-" : "+-");
    os << m_type << ", " << m_size << " GiB" << endl;
    unsigned long i = 0;
    unsigned long size = m_partitions.size();
    if(size == 0)
        return;
    prepend += (isLast ? "  " : "| ");
    size--;
    for(const auto & partition: m_partitions){
        os << prepend << (i == size  ? "\\-" : "+-");
        os << "[" << i << "]: ";
        os << partition.size << " GiB, " <<  partition.name << endl;
        i++;
    }
}

class CMemory: public CComponent {
    int m_size;
public:
    CMemory(int size);
    void Print(ostream &os, const map<int, bool> &levels, bool isLast) const override;
};

CMemory::CMemory(int size): m_size(size) {

}

void CMemory::Print(ostream &os, const map<int, bool> &levels, bool isLast) const {
    for(auto & level : levels){
        os << (level.second ? "| " : "  ");
    }
    os << (isLast ? "\\-" : "+-");
    os << "Memory, " << m_size << " MiB" << endl;
}

class CComputer {
    list<shared_ptr<CComponent>> m_components;
    string m_address;
    list<shared_ptr<CAddress>> m_addresses;
public:
    CComputer(const string & address);
    CComputer &AddAddress(const string& address);
    CComputer &AddComponent(const CCPU& component);
    CComputer &AddComponent(const CMemory& component);
    CComputer &AddComponent(const CDisk& component);
    bool hasAddress(const string& query) const;
    friend ostream& operator << (ostream & os, const CComputer& computer);
    void Print(ostream & os, bool isSub = false, bool isLast = false) const;
};

CComputer::CComputer(const string &address): m_address(address) {
}

CComputer &CComputer::AddAddress(const string &address) {
    m_addresses.push_back(make_shared<CAddress>(address));
    return *this;
}

CComputer &CComputer::AddComponent(const CCPU & component) {
    m_components.push_back(make_shared<CCPU>(component));
    return *this;
}

CComputer &CComputer::AddComponent(const CMemory & component) {
    m_components.push_back(make_shared<CMemory>(component));
    return *this;
}

CComputer &CComputer::AddComponent(const CDisk & component) {
    m_components.push_back(make_shared<CDisk>(component));
    return *this;
}

ostream &operator<<(ostream &os, const CComputer &computer) {
    computer.Print(os);
    return os;
}

void CComputer::Print(ostream &os, bool isSub, bool isLast) const {
    map<int,bool> levels;
    if(isSub){
        levels.insert(make_pair(1,!isLast));
        os << (isLast ? "\\-" : "+-");
    }
    os << "Host: " << m_address << endl;
    unsigned long addr_size = m_addresses.size();
    addr_size--;
    unsigned long i = 0;
    for(auto & comp : m_addresses)
        comp->Print(os,levels,addr_size == i++ && m_components.size() == 0);
//
    unsigned long size = m_components.size();
    size--;
    unsigned long j = 0;
    for(auto & comp : m_components)
        comp->Print(os,levels,size == j++);
}

bool CComputer::hasAddress(const string &query) const {
    if(query == m_address)
        return true;
    for(const auto & addr : m_addresses)
        if(addr->getName() == m_address)
            return true;
    return false;
}


class CNetwork {
    list<CComputer> m_computers;
    string m_address;
public:
    CNetwork(const string & address);
    CNetwork &AddComputer(const CComputer& computer);
    CComputer *FindComputer(const string& query);
    friend ostream& operator << (ostream & os, const CNetwork& network);
};

CNetwork::CNetwork(const string &address): m_address(address) {

}

CNetwork &CNetwork::AddComputer(const CComputer &computer) {
    m_computers.push_back(computer);
    return *this;
}

CComputer *CNetwork::FindComputer(const string &query) {
    for(auto & comp : m_computers)
        if(comp.hasAddress(query))
            return &comp;
    return nullptr;
}

ostream &operator<<(ostream &os, const CNetwork& network) {
    os << "Network: " << network.m_address << endl;
    unsigned long size = network.m_computers.size();
    unsigned long i = 0;
    for(auto & computer : network.m_computers)
        computer.Print(os,true, size == ++i);
    return os;
}


#ifndef __PROGTEST__
template<typename _T>
string toString ( const _T & x )
{
    ostringstream oss;
    oss << x;
    return oss . str ();
}

int main ( void )
{
    CNetwork n ( "FIT network" );
    n . AddComputer (
            CComputer ( "progtest.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.142" ) .
                    AddComponent ( CCPU ( 8, 2400 ) ) .
                    AddComponent ( CCPU ( 8, 1200 ) ) .
                    AddComponent ( CDisk ( CDisk::MAGNETIC, 1500 ) .
                    AddPartition ( 50, "/" ) .
                    AddPartition ( 5, "/boot" ).
                    AddPartition ( 1000, "/var" ) ) .
                    AddComponent ( CDisk ( CDisk::SSD, 60 ) .
                    AddPartition ( 60, "/data" )  ) .
                    AddComponent ( CMemory ( 2000 ) ).
                    AddComponent ( CMemory ( 2000 ) ) ) .
            AddComputer (
            CComputer ( "edux.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.158" ) .
                    AddComponent ( CCPU ( 4, 1600 ) ) .
                    AddComponent ( CMemory ( 4000 ) ).
                    AddComponent ( CDisk ( CDisk::MAGNETIC, 2000 ) .
                    AddPartition ( 100, "/" )   .
                    AddPartition ( 1900, "/data" ) ) ) .
            AddComputer (
            CComputer ( "imap.fit.cvut.cz" ) .
                    AddAddress ( "147.32.232.238" ) .
                    AddComponent ( CCPU ( 4, 2500 ) ) .
                    AddAddress ( "2001:718:2:2901::238" ) .
                    AddComponent ( CMemory ( 8000 ) ) );
    assert ( toString ( n ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  \\-Memory, 8000 MiB\n" );
    CNetwork x = n;
    auto c = x . FindComputer ( "imap.fit.cvut.cz" );
    assert ( toString ( *c ) ==
             "Host: imap.fit.cvut.cz\n"
             "+-147.32.232.238\n"
             "+-2001:718:2:2901::238\n"
             "+-CPU, 4 cores @ 2500MHz\n"
             "\\-Memory, 8000 MiB\n" );
    c -> AddComponent ( CDisk ( CDisk::MAGNETIC, 1000 ) .
            AddPartition ( 100, "system" ) .
            AddPartition ( 200, "WWW" ) .
            AddPartition ( 700, "mail" ) );
    assert ( toString ( x ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  +-Memory, 8000 MiB\n"
             "  \\-HDD, 1000 GiB\n"
             "    +-[0]: 100 GiB, system\n"
             "    +-[1]: 200 GiB, WWW\n"
             "    \\-[2]: 700 GiB, mail\n" );
    assert ( toString ( n ) ==
             "Network: FIT network\n"
             "+-Host: progtest.fit.cvut.cz\n"
             "| +-147.32.232.142\n"
             "| +-CPU, 8 cores @ 2400MHz\n"
             "| +-CPU, 8 cores @ 1200MHz\n"
             "| +-HDD, 1500 GiB\n"
             "| | +-[0]: 50 GiB, /\n"
             "| | +-[1]: 5 GiB, /boot\n"
             "| | \\-[2]: 1000 GiB, /var\n"
             "| +-SSD, 60 GiB\n"
             "| | \\-[0]: 60 GiB, /data\n"
             "| +-Memory, 2000 MiB\n"
             "| \\-Memory, 2000 MiB\n"
             "+-Host: edux.fit.cvut.cz\n"
             "| +-147.32.232.158\n"
             "| +-CPU, 4 cores @ 1600MHz\n"
             "| +-Memory, 4000 MiB\n"
             "| \\-HDD, 2000 GiB\n"
             "|   +-[0]: 100 GiB, /\n"
             "|   \\-[1]: 1900 GiB, /data\n"
             "\\-Host: imap.fit.cvut.cz\n"
             "  +-147.32.232.238\n"
             "  +-2001:718:2:2901::238\n"
             "  +-CPU, 4 cores @ 2500MHz\n"
             "  \\-Memory, 8000 MiB\n" );
    return 0;
}
#endif /* __PROGTEST__ */
